
import * as admin from 'firebase-admin'
import * as functions from 'firebase-functions'

admin.initializeApp()

const db = admin.firestore()
const fcm = admin.messaging()

async function asyncForEach(array: Array<any>, callback: (item: any) => Promise<void>): Promise<void> {
  for (const element of array) {
    await callback(element)
  }
}

exports.dailyTotal = functions
            .region('europe-west1')
            .pubsub
            .schedule('every 2 minutes')
            .timeZone('Europe/Warsaw') 
            .onRun(async () => {

  const today = admin.firestore.Timestamp.now().toDate()
  const todayStr = today.toISOString().split('T')[0]

  const users = await db.collection('users').get()
  const usersIds = users.docs.map(snap => snap.id)

  asyncForEach(usersIds, async (userId: string): Promise<void> => {
    console.log('USER ID', userId)
    try {
      const timeseries = await db.collection('users')
        .doc(userId)
        .collection('steps')
        .doc('_timeseries')
        .get()

      interface Steps {
        datetime: string,
        steps: number
      }

      const allStepsData: Array<Steps> = timeseries.data()['data']
      let todayTotal = 0

      if (allStepsData.length !== 0) {
        const todaySteps: Array<number> = allStepsData
          .filter(({ datetime }) => datetime.split(" ")[0] === todayStr)
          .map(obj => obj['steps'])
        if (todaySteps.length !== 0) {
          todayTotal = todaySteps[todaySteps.length - 1] - todaySteps[0]
          todaySteps.reduce((prev, curr) => {
            if (prev > curr) {
              todayTotal += prev
            }
            return curr
          })
        }
        
        await db.collection('users')
          .doc(userId)
          .collection('steps')
          .doc('daily')
          .set({ [todayStr]: todayTotal }, { merge: true })
      }
    } catch (error) {
      console.log('ERROR:', error, 'UserID:', userId)
    }
  })
})


exports.progressUpdate = functions
            .region('europe-west1')
            .pubsub
            .schedule('every 5 minutes')
            .timeZone('Europe/Warsaw')
            .onRun(async () => {
    const users = await db.collection('users').get()
    const usersIds = users.docs.map(snap => snap.id)

    const today = admin.firestore.Timestamp.now().toDate()
    const todayStr = today.toISOString().split('T')[0]
    const yesterday = new Date(today.setDate(today.getDate() - 1))
    const yesterdayStr = yesterday.toISOString().split('T')[0]
    const tomorrow = new Date(yesterday.setDate(yesterday.getDate() + 2))
    const tomorrowStr = tomorrow.toISOString().split('T')[0]

    asyncForEach(usersIds, async (userId: string): Promise<void> => {
      console.log('USER_ID', userId)
      // STEPS DATA
      interface Steps {
        datetime: string,
        steps: number
      }

      const stepsTimeseries = await db.collection('users')
        .doc(userId)
        .collection('steps')
        .doc('_timeseries')
        .get()
      const allUserSteps: Array<Steps> = stepsTimeseries.data()['data']

      // CHALLENGES DATA
      interface Challenge {
        activatedWhen: string,
        challengeId: string,
        progressMap: {
          [date: string]: number
        },
        dailyAmount: number
      }

      const activeChallenges = await db.collection('users')
        .doc(userId)
        .collection('challenges')
        .where('state', '==', 'in_progress')
        .get()
      const activeChallengesData: Array<Challenge> = []

      activeChallenges.forEach(snap => {
        const challengeId = snap.id
        const { activatedWhen, goal: { progress, dailyAmount } } = snap.data()
        activeChallengesData.push({
          activatedWhen: activatedWhen,
          challengeId: challengeId,
          progressMap: progress,
          dailyAmount: dailyAmount
        })
      })
      console.log('Active challenges', activeChallengesData)

      if (activeChallengesData.length !== 0) {
        asyncForEach(activeChallengesData, async (challengeObj: Challenge): Promise<void> => {
          const { activatedWhen, challengeId, progressMap, dailyAmount }: Challenge = challengeObj
          const challenge = db.collection('users')
            .doc(userId)
            .collection('challenges')
            .doc(challengeId)
          let state: string = 'in_progress'
          let challengeTotal = 0

          const todaySteps: Array<number> = allUserSteps
            .filter(({ datetime }) => {
              const stepsDateTime = new Date(datetime)
              const activatedDateTime = new Date(activatedWhen)
              return datetime.split(" ")[0] === todayStr
                && stepsDateTime >= activatedDateTime
            })
            .map(obj => obj['steps'])

          console.log('today steps after challenge activation', todaySteps)
          if (todaySteps.length !== 0) {
            challengeTotal = todaySteps[todaySteps.length - 1] - todaySteps[0]
            todaySteps.reduce((prev, curr) => {
              if (prev > curr) {
                challengeTotal += prev
              }
              return curr
            })
            console.log('challengeTotal', challengeTotal)
          }

          if (progressMap.hasOwnProperty(yesterdayStr) && progressMap[yesterdayStr] < 100) {
            state = 'failed'
            await challenge.update({ state: state })
          } else if (progressMap.hasOwnProperty(todayStr)) {
            let todayProgress: number = Math.round((challengeTotal / dailyAmount) * 100)
            console.log('todayProgres', todayProgress)
            if (todayProgress > 100) {
              todayProgress = 100
              if (!progressMap.hasOwnProperty(tomorrowStr)) {
                state = 'completed'
              }
            }
            await challenge.set({ goal: { progress: { [todayStr]: todayProgress } }, state: state }, { merge: true })
          }

          if (['failed', 'completed'].includes(state)) {
            const challengeData = await challenge.get()
            const challengeDataObj = challengeData.data()
            const token = await db.collection('users')
              .doc(userId)
              .collection('tokens')
              .limit(1)
              .get()
            const tokenStr = token.docs[0].id
            await db.collection('notifications')
              .add({ challenge: challengeDataObj, token: tokenStr })
          }
        })
      }
    }
    )
  })


exports.sendNotifications = functions
              .region('europe-west1')
              .pubsub
              .schedule('every 2 minutes from 07:00 to 23:00')
              .timeZone('Europe/Warsaw') 
              .onRun(async () => {

  const notificationsQueue = await db.collection('notifications').get()
  const notificationsArray = notificationsQueue.docs

  if (notificationsArray.length !== 0) {
    asyncForEach(notificationsArray, async snap => {
      const notificationId = snap.id
      const token = snap.data()['token']
      const state = snap.data()['challenge']['state']

      interface notificationData {
        title: string,
        notification: {
          completed: {
            body: string,
            description: string,
            title: string
          },
          failed: {
            body: string,
            description: string,
            title: string
          }
        }
      }

      const { notification: { completed: completedData, failed: failedData}, title: challenge_name}: notificationData = snap.data()['challenge']
      let payload: { title: string, body: string, description: string }

      if (state === 'completed') {
        payload = completedData
      }
      else {
        payload = failedData
      }

      const { title, body, description } = payload

      interface notificationPayload {
        notification: {
          title: string,
          body: string,
          click_action: string
        },
        data: {
          state: string,
          challenge_name: string,
          description: string,
          click_action: string
        }
      }

      const notificationPayload: notificationPayload = {
        notification: {
          title: title,
          body: body,
          click_action: 'FLUTTER_NOTIFICATION_CLICK'
        },
        data: {
          state: state,
          challenge_name: challenge_name,
          description: description,
          click_action: 'FLUTTER_NOTIFICATION_CLICK'
        }
      }

      await db.collection('notifications').doc(notificationId).delete()
      const response = await fcm.sendToDevice(token, notificationPayload)
      const result = response.results[0]
      const error = result.error
      if (error) {
        console.error('Failure sending notification to:', token, error)
      }
    })
  }
})


// DATA EXPORT TO GOOGLE SPREADSHEET
import { google } from 'googleapis'

const CLIENT_ID = functions.config().googleapi.client_id
const SECRET_KEY = functions.config().googleapi.client_secret
const REDIRECT_URI = 'https://europe-west1-hls-aiden.cloudfunctions.net/OauthCallback'
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

const oAuth2Client = new google.auth.OAuth2(CLIENT_ID, SECRET_KEY, REDIRECT_URI)
const authorizeUrl = oAuth2Client.generateAuthUrl({
  access_type: 'offline',
  scope: SCOPES,
  prompt: 'consent'
})

exports.authGoogleAPI = functions
              .region('europe-west1')
              .https
              .onRequest((req, res) => {
  res.redirect(authorizeUrl)
})

exports.OauthCallback = functions
              .region('europe-west1')
              .https
              .onRequest((req, res) => {
  const auth_code = req.query.code
  oAuth2Client.getToken(auth_code, (err, tokens) => {
    if (err) {
      return res.status(400).send(err)
    }
    return db.collection('google_api_tokens').add(tokens).then(() => res.status(200).send('OK'))
  })
})

exports.exportToSpreadsheet = functions
              .region('europe-west1')
              .pubsub
              .schedule('00 00 * * *')
              .timeZone('Europe/Warsaw')
              .onRun(async () => {
  const SHEET_ID = '1BAlReRZefmEfM4vKjXFisnEsqgdRKL7mmlmzMrfMMqY'
  const sheets = google.sheets('v4')
  const oauthTokens = await db.collection('google_api_tokens').get()
  const token = oauthTokens.docs[0].data()
  oAuth2Client.setCredentials(token)

  interface Unwrapped {
    userId: string,
    [key: string]: unknown
  }
  
  // UTILS
  function unwrap(container: Unwrapped, nestedObject: object, suffix: string = null) {
    for (const [key, value] of Object.entries(nestedObject)) {
      if (typeof value !== 'object' || value === null || Array.isArray(value)) {
        if (suffix) {
          container[`${suffix}_${key}`] = value
        } else {
          container[key] = value
        }
      } else {
        if (suffix) {
          unwrap(container, value, `${suffix}_${key}`)
        } else {
          unwrap(container, value, key)
        }
      }
    }
    return container
  }

  async function clearAndAppend(range: string, values: Array<Array<any>>, auth: any = oAuth2Client, sheetId: string = SHEET_ID): Promise<void> {
    await sheets.spreadsheets.values.clear({
      spreadsheetId: sheetId,
      range: range,
      auth: auth
    })

    const request = {
      spreadsheetId: sheetId,
      range: range,
      valueInputOption: 'USER_ENTERED',
      insertDataOption: 'OVERWRITE',
      resource: {
        values: values
      },
      auth: auth
    }

    sheets.spreadsheets.values.append(request, (err: unknown, response: unknown) => {
      if (err) {
        console.error(err)
        return
      }
      console.log(JSON.stringify(response, null, 2))
    })
  }

  // USERS
  const users = await db.collection('users').get()
  const usersIds = users.docs.map(snap => snap.id)

  // USERS MAIN DATA EXPORT
  const usersMain: Array<Unwrapped> = users.docs.map(snap => {
    const userId = snap.id
    const userData = snap.data()
    if (userData.hasOwnProperty('createdWhen')) {
      userData.createdWhen = userData.createdWhen.toDate().toISOString()
    }
    if (userData.hasOwnProperty('device')) {
      userData.device.systemFeatures = userData.device.systemFeatures.toString()
    }
    return unwrap({userId: userId}, userData)
  })

  const columnsMain = Object.keys(usersMain[0])
  const rowsMain = usersMain.map((obj) => {
    const row = Array.from({ length: columnsMain.length })
    for (const [key, value] of Object.entries(obj)) {
      const index = columnsMain.indexOf(key)
      row[index] = value
    }
    return row
  })
  const exportMain = [columnsMain, ...rowsMain]
  clearAndAppend('Users!A:Z', exportMain)

  // CHALLENGES EXPORT
  const usersChallenges: Array<Unwrapped> = []

  await asyncForEach(usersIds, async (userId: string): Promise<void> => {
    const challenges = await db.collection('users').doc(userId).collection('challenges').get()
    const challengesUnwrapped = challenges.docs
        .filter(snap => snap.id !== 'delete-me')
        .map(snap => {
      const challengeId = snap.id
      const challengeData = snap.data()
      const progressMap: { [key: string]: number } = challengeData.goal.progress
      const progress: Array<[string, number]> = []
      for (const [key, value] of Object.entries(progressMap)) {
        progress.push([key, value])
      }
      const progressStr = progress.toString()
      challengeData.goal.progress = progressStr
      return unwrap({ userId: userId, challengeId: challengeId }, challengeData)
    })
    usersChallenges.push(...challengesUnwrapped)
  })
      
  const columnsChallenges = Object.keys(usersChallenges[0])
  const rowsChallenges = usersChallenges.map((obj) => {
    const row = Array.from({ length: columnsChallenges.length })
    for (const [key, value] of Object.entries(obj)) {
      const index = columnsChallenges.indexOf(key)
      row[index] = value
    }
    return row
  })

  const exportChallenges = [columnsChallenges, ...rowsChallenges]
  clearAndAppend('Challenges!A:Z', exportChallenges)

  // DAILY STEPS EXPORT
  const usersSteps: Array<Array<number | string>> = []

  await asyncForEach(usersIds, async (userId: string): Promise<void> => {
    console.log('USERID DAILY', userId)
    const stepsDaily = await db.collection('users').doc(userId).collection('steps').doc('daily').get()
    if (stepsDaily.exists) {
      const stepsDailyMap = stepsDaily.data()
      const columns = []
      const row = []
      for (const [date, steps] of Object.entries(stepsDailyMap)) {
        columns.push(date)
        row.push(steps)
      }
      columns.unshift('userId')
      row.unshift(userId)
      usersSteps.push(columns, row)
    }
  })
  
  clearAndAppend('Steps!A:Z', usersSteps)

  // FITDATA EXPORT
  const fitDataColumns = ['userId', 'dataType', 'dateFrom', 'dateTo', 'source', 'userEntered', 'value']
  const fitDataRows: Array<Array<any>> = []

  await asyncForEach(usersIds, async (userId: string): Promise<void> => {
    console.log('USERID FITDATA', userId)
    const fitData = await db.collection('users').doc(userId).collection('fit_data').doc('data').get()
    if (Object.keys(fitData.data()).length !== 0) {
      const fitDataMap: {[key: string]: Array<{[key: string]: any}>} = fitData.data()['DataType']
      for (const [dataType, dataArray] of Object.entries(fitDataMap)) {
        const dataTypeRows = dataArray.map(obj => {
          obj.dateFrom = obj.dateFrom.toDate().toISOString()
          obj.dateTo = obj.dateTo.toDate().toISOString()
          const singleRow = Array.from({length: fitDataColumns.length})
          singleRow[0] = userId
          singleRow[1] = dataType
          for (const [key, value] of Object.entries(obj)) {
            const index = fitDataColumns.indexOf(key)
            singleRow[index] = value
          }
          return singleRow
        })
        fitDataRows.push(...dataTypeRows)
      }
    }
  })

  const exportFitData = [fitDataColumns, ...fitDataRows]
  clearAndAppend('Fit_data!A:Z', exportFitData)


  // LOCATION DATA EXPORT
  const locationColumns = ['userId', 'dateTime', 'altitude', 'latitude', 'longitude', 'speed']
  const locationRows: Array<Array<number>> = []
  await asyncForEach(usersIds, async (userId: string): Promise<void> => {
    console.log('USERID LOCATION', userId)
    const location = await db.collection('users').doc(userId).collection('location_data').doc('data').get()
    const locationData = location.data()
    for (const [dateTime, locationMap] of Object.entries(locationData)) {
      const locationRow: Array<any> = Array.from({length: locationColumns.length})
      locationRow[0] = userId
      locationRow[1] = dateTime
      for (const [key, value] of Object.entries(locationMap)) {
        const index = locationColumns.indexOf(key)
        locationRow[index] = value
      }
      locationRows.push(locationRow)
    }
  })

  const exportLocationData = [locationColumns, ...locationRows]
  clearAndAppend('Location_data!A:Z', exportLocationData)
})